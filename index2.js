"use strict";

function isPrime(num) {
  if (num < 2) {
    return false;
  }
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}

let m;
let n;

while (true) {
  m = Number(prompt("Введіть меньше число:"));
  n = Number(prompt("Введіть більше число:"));

  if (Number.isInteger(n) && Number.isInteger(m) && m < n) {
    break;
  } else {
    alert("Переконайтесь, що ви вірно ввели числа!!!");
  }
}

for (let i = m; i <= n; i++) {
  if (isPrime(i)) {
    console.log(i);
  }
}
